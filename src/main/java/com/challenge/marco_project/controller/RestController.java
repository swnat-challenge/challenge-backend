package com.challenge.marco_project.controller;


import com.challenge.marco_project.algorithmsChallenge.Fibonacci;
import com.challenge.marco_project.exception.ResourceNotFoundException;
import com.challenge.marco_project.model.Patient;
import com.challenge.marco_project.model.VitalSigns;
import com.challenge.marco_project.pojo.PatientData;
import com.challenge.marco_project.repository.PatientRepository;
import com.challenge.marco_project.repository.VitalSignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marco on 21/09/18.
 */

@org.springframework.web.bind.annotation.RestController
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
public class RestController {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private VitalSignRepository vitalSignRepository;

    @Autowired
    private Fibonacci fibonacci;

    @GetMapping("/fibonacci/{fib}")
    public Long[] getFibonacci(@PathVariable Long fib)
    {
        return fibonacci.fibonacci(fib);
    }

    @GetMapping("/patients")
    public Page<Patient> getPatients(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    @GetMapping("/patients/data")
    public List<PatientData> getPatientsData() {
        List<PatientData> lista = new ArrayList<>();
        List<Patient> patients = patientRepository.findAll();
        for(Patient p: patients){
            PatientData pd = new PatientData();
            pd.setId(p.getId());
            pd.setName(p.getName());
            pd.setAge(p.getAge());
            VitalSigns v = vitalSignRepository.findLatestVitalSigns(p.getId());
            if(v != null && v.getId() != null){
                pd.setHeartRate(v.getHeartRate());
                pd.setSystolic(v.getSystolic());
                pd.setDiastolic(v.getDiastolic());
            }
            lista.add(pd);
        }
        return lista;
    }

    @GetMapping("/patient/{name}")
    public Patient getPatient(@PathVariable String name) {
        return patientRepository.findByName(name);
    }

    @GetMapping("/vitalSign/{patientId}")
    public VitalSigns getLatestVitalSignsFromPatient(@PathVariable Long patientId) {
        return vitalSignRepository.findLatestVitalSigns(patientId);
    }

    @PostMapping("/patients")
    public Patient createPatient(@Valid @RequestBody Patient patient) {
        return patientRepository.save(patient);
    }

    @PutMapping("/patients/{patientId}")
    public Patient updatePatient(@PathVariable Long patientId,
                                   @Valid @RequestBody Patient patientRequest) {
        return patientRepository.findById(patientId)
                .map(patient -> {
                    patient.setName(patientRequest.getName());
                    patient.setAge(patientRequest.getAge());
                    return patientRepository.save(patient);
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));
    }


    @DeleteMapping("/patients/{patientId}")
    public ResponseEntity<?> deletePatient(@PathVariable Long patientId) {
        return patientRepository.findById(patientId)
                .map(patient -> {
                    patientRepository.delete(patient);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));
    }

    @PostMapping("/patients/{patientId}/vitalSigns")
    public @Valid VitalSigns addVitalSignsToPatient(@PathVariable Long patientId, @RequestBody @Valid VitalSigns vitalSigns) {
        return patientRepository.findById(patientId)
                .map(patient -> {
                    vitalSigns.setPatient(patient);
                    return vitalSignRepository.save(vitalSigns);
                }).orElseThrow(() -> new ResourceNotFoundException("Patient not found with id " + patientId));

    }

    @GetMapping("/patients/{patientId}/vitalSigns")
    public List<VitalSigns> getVitalsSignsByPatientsId(@PathVariable Long patientId) {
        return vitalSignRepository.findByPatientId(patientId);
    }


    @PutMapping("/patients/{patientId}/vitalSigns/{vitalSignsId}")
    public VitalSigns updateVitalSigns(@PathVariable Long patientId,
                                              @PathVariable Long vitalSignsId,
                                              @Valid @RequestBody VitalSigns vitalSignsRequest) {
        if(!patientRepository.existsById(patientId)) {
            throw new ResourceNotFoundException("Patient not found with id " + patientId);
        }

        return vitalSignRepository.findById(vitalSignsId)
                .map(vitalSigns -> {
                    vitalSigns.setHeartRate(vitalSignsRequest.getHeartRate());
                    vitalSigns.setDiastolic(vitalSignsRequest.getDiastolic());
                    vitalSigns.setSystolic(vitalSignsRequest.getSystolic());
                    return vitalSignRepository.save(vitalSigns);
                }).orElseThrow(() -> new ResourceNotFoundException("Vital Signs not found with id " + vitalSignsId));

    }

    @DeleteMapping("/patients/{patientId}/vitalSigns/{vitalSignsId}")
    public ResponseEntity<?> deleteVitalSigns(@PathVariable Long patientId,
                                          @PathVariable Long vitalSignsId) {
        if(!patientRepository.existsById(patientId)) {
            throw new ResourceNotFoundException("Patient not found with id " + patientId);
        }

        return vitalSignRepository.findById(vitalSignsId)
                .map(vitalSignst -> {
                    vitalSignRepository.delete(vitalSignst);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Vital Signs not found with id " + vitalSignsId));

    }



}

